[[ HTML ]](<./HTML.md>) | \[ JavaScript \] | [[ Perl ]](<./Perl.md>) | [[ Xslate ]](<./Xslate.md>) | [[ XHTML ]](<./XHTML.md>)

# JavaScript

## Trigger: `each`

~~~~
each(function(i, e) {
    $0
});
~~~~

## Trigger: `func`

~~~~
function$1($2) {
    $0
}
~~~~

## Trigger: `log`

~~~~
console.log(${1:'$2'});
~~~~

## Trigger: `switch`

~~~~
switch (${1:expression}) {
    case ${2:label1}:
        ${3:statements1;}
    break;
    case label2:
        statements2;
    break;
    default:
        
    break;
}
~~~~
