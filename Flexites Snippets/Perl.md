[[ HTML ]](<./HTML.md>) | [[ JavaScript ]](<./JS.md>) | \[ Perl \] | [[ Xslate ]](<./Xslate.md>) | [[ XHTML ]](<./XHTML.md>)

# Perl

# Trigger: `app`

~~~~
\$App->Param('$1'${2:, '$3'})
~~~~

~~~~
\$App->UndefParam('$1')
~~~~

~~~~
\$App->Data->Var('$1'${2:, '$3'})
~~~~

~~~~
\$App->Data->UndefVar('$1')
~~~~

~~~~
\$App->Session->Var('$1'${2:, '$3'})
~~~~

~~~~
\$App->Session->UndefVar('$1')
~~~~

~~~~
\$App->Session->UID()
~~~~

~~~~
\$App->Settings->GetParamValue('$1')
~~~~

# Trigger: `cat`

~~~~
CAT_MEGAGET(
    parent_id    => ${1:\$hash->\{'object_id'\}},
    class_name   => ['${2:page_with_form}'${3}],
    attrs        => [${4}],
    use_publ_pos => 1
)
~~~~

~~~~
CAT_GET_OBJECT(object_id => ${1:\$hash->\{'object_id'\}})
~~~~

~~~~
CAT_GET_OBJECT_WITH_ATTRS(
    object_id => ${1:\$hash->\{'object_id'\}},
    attrs     => ['${2:picture}']
)
~~~~

~~~~
CAT_GET_OBJECT_BY_CLASS_NAME(class_name => '${1:page}'${2:, parent_id => ${3:\$hash->\{'object_id'\}}})
~~~~

~~~~
CAT_GET_OBJECTS_BY_CLASS_NAME(class_name => '${1:page}'${2:, parent_id => ${3:\$hash->\{'object_id'\}}})
~~~~

~~~~
CAT_GET_OBJECT_ATTR(
    object_id   => ${1:\$hash->\{'object_id'\}},
    attr_name   => '${2:content}',
    result_type => 'sql_res'
)
~~~~

~~~~
CAT_GET_OBJECT_ATTR(
    object_id   => ${1:\$hash->\{'object_id'\}},
    attr_name   => '${2:content}',
    result_type => 'value'
)
~~~~

# Trigger: `fetch`

~~~~
fetchrow_hashref()
~~~~

~~~~
fetchall_arrayref({})
~~~~

# Trigger: `hash`

~~~~
\$hash->{'${1:object_id}'}
~~~~
