[[ HTML ]](<./HTML.md>) | [[ JavaScript ]](<./JS.md>) | [[ Perl ]](<./Perl.md>) | \[ Xslate \] | [[ XHTML ]](<./XHTML.md>)

# Xslate

## Trigger: `<:`

~~~~
<: $0 :>
~~~~

## Trigger: `if`

~~~~
: if \$${1:item} {
    $0
: }
~~~~

## Trigger: `for`

~~~~
: for \$${1:object.${2:items}} -> \$${3:item} {
    $0
: }
~~~~

## Trigger: `given`

~~~~
: given \$${1:item} {
    : when '$2' {
        $0
    : }
    : default {

    : }
: }
~~~~

## Trigger: `include`

~~~~
: include includes::${1:block}${2: \{ ${3:name} => ${4:value} \}}
~~~~
