\[ HTML \] | [[ JavaScript ]](<./JS.md>) | [[ Perl ]](<./Perl.md>) | [[ Xslate ]](<./Xslate.md>) | [[ XHTML ]](<./XHTML.md>)

# HTML

## Trigger: `div`

~~~~
<div class="${1:block}">
    $0
</div>
~~~~

## Trigger: `ul`

~~~~
<ul class="${1:block}">
    $0
</ul>
~~~~

## Trigger: `li`

~~~~
<li class="${1:${2:block}__${3:item}}">
    $0
</li>
~~~~

## Trigger: `table`

~~~~
<table>
    <thead>
        <tr>
            <td>$0</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
        </tr>
    </tfoot>
</table>
~~~~

## Trigger: `favicon`

~~~~
<link rel="shortcut icon" type="image/png" href="/i/favicon.png" />
~~~~

~~~~
<link rel="shortcut icon" type="image/x-icon" href="/i/favicon.ico" />
~~~~

~~~~
<link rel="shortcut icon" type="image/gif" href="/i/favicon.gif" />
~~~~
