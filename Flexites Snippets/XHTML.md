[[ HTML ]](<./HTML.md>) | [[ JavaScript ]](<./JS.md>) | [[ Perl ]](<./Perl.md>) | [[ Xslate ]](<./Xslate.md>) | \[ XHTML \]

# XHTML

## Trigger: `_class`

~~~~
_class="Objects::Tag" _params="${1:class}"
~~~~

## Trigger: `a`

~~~~
<a _params="${1:object_id} as node_id">${2:<param name="${3:object_name}" />}</a>
~~~~

## Trigger: `li`

~~~~
<li _class="Objects::Tag" _params="${1:class}">
    $0
</li>
~~~~

## Trigger: `include`

~~~~
<!--#include virtual="templates/includes/${1:filename}.xhtml"-->
~~~~

## Trigger: `param`

~~~~
<param name="${1:object_name}" />
~~~~

~~~~
<param expr="" />
~~~~

## Trigger: `getimg`

~~~~
<getimg file="${1:picture}" src="${2:small_}src" title="object_name" />
~~~~

## Trigger: `pdate`

~~~~
<pdate name="${1:publication_date}" template="${2:%d %RU_MONTH %Y �.}" />
~~~~

## Trigger: `exec`

~~~~
<exec expr="$0" />
~~~~

## Trigger: `execsql`

~~~~
<execsql sql="${1}" _style="2">
    $0
</execsql>
~~~~

## Trigger: `if`

~~~~
<if predicate="${1:\$hash->\{'object_id'\}}$2">
    $0
</if>
~~~~

## Trigger: `news`

~~~~
<news sql="$1" _style="2">
    $0
</news>
~~~~

~~~~
<news expr="$1" _style="2">
    $0
</news>
~~~~

## Trigger: `intag`

~~~~
<intag _tag="${1:ul}"${2: class="${3:block}"} />
~~~~

## Trigger: `struct`

~~~~
<struct>
    $0
</struct>
~~~~

## Trigger: `guts`

~~~~
<guts />
~~~~

## Trigger: `switch`

~~~~
<switch value="${1:\$hash->\{'_COUNTER'\} == 1}">
    <if>
        $0
    </if>
    <else>

    </else>
</switch>
~~~~

~~~~
<switch value="${1:\$hash->\{'class_name'\}}">
    <case value="'home_page'">
        $0
    </case>
    <default>

    </default>
</switch>
~~~~

## Trigger: `com`

~~~~
<if predicate="0"><!--
~~~~

## Trigger: `cut`

~~~~
--></if>
~~~~

## Trigger: `comment`

~~~~
<if predicate="0"><!--
    $0
--></if>
~~~~
