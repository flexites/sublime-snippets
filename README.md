# README #

В этом репозитории собраны сниппеты для работы с движком Flexites с использованием Sublime Text

### Для чего это нужно ###

* Быстрое выполнение базовых вещей (стараемся избавиться от рутины)

### Установка ###

* В sublime text открываем Tools > Developer > New Snippet. Сразу переходим к сохранению файла (так мы узнаем, в какую папку добавляются сниппеты)
* Переходим в эту папку и вставляем все файлы сниппета из репозитория
* После этого ими сразу можно пользоваться. Например, в файле шаблонизатора (расширение .tx) при вводе "tx_" и нажатии комбинации Ctrl+Space (Windows, OS X) или Alt+/ (Linux) откроются все возможные для шаблонизатора команды

### Добавление новых сниппетов ###

* Используйте перфиксы (tx_, pm_, ...) при создании новых сниппетов. Так можно избежать разночтений
* Желательно также указывать scope (область применения)